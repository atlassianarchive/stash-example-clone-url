package com.atlassian.clone.name;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.stash.ssh.api.SshKeyService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;

import java.util.Map;

/**
 * Conditionally display web fragments based whether the context user has set at least one SSH key.
 *
 * @since 1.1
 */
public class UserHasSshKeyCondition implements Condition {

    private final SshKeyService sshKeyService;
    private final StashAuthenticationContext authenticationContext;

    private boolean negate;

    public UserHasSshKeyCondition(SshKeyService sshKeyService, StashAuthenticationContext authenticationContext) {
        this.sshKeyService = sshKeyService;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
        negate = Boolean.valueOf(params.get("negate"));
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> context) {
        StashUser currentUser = authenticationContext.getCurrentUser();
        if (currentUser == null) {
            // anon users can not have SSH keys
            return false;
        }

        return negate != sshKeyService.hasSshKey(currentUser);
    }
}
