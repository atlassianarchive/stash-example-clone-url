package com.atlassian.clone.name;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.stash.ssh.api.SshCloneUrlResolver;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * Simple context for rendering the SSH clone URL in the UI.
 */
public class SshCloneUrlContextProvider implements ContextProvider {

    private final SshCloneUrlResolver sshCloneUrlResolver;

    public SshCloneUrlContextProvider(SshCloneUrlResolver sshCloneUrlResolver) {
        this.sshCloneUrlResolver = sshCloneUrlResolver;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        return ImmutableMap.<String, Object>builder()
                .putAll(context)
                .put("sshCloneUrlResolver", sshCloneUrlResolver)
                .build();
    }

}
